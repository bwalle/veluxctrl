#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import time
import os
import cgi
import math
import time
import datetime
import json
import optparse
import sys
import subprocess
import xml.dom.minidom as minidom
import urllib.request as urlrequest
import ssl

PORT = '/dev/ttyACM0'
DRY_RUN = False
WRITE_DEBUG_LOG = False
VELUXCTRL_BASE = '/data/veluxctrl'
EVENT_LOG = os.path.join(VELUXCTRL_BASE, 'events.log')
DEBUG_LOG = os.path.join(VELUXCTRL_BASE, 'debug.log')
CONFIGURATION_FILE = os.path.join(VELUXCTRL_BASE, 'config.json')
WEIHERHOF_URL = 'http://forecast.io/#/f/49.4558,10.9274?units=si'

WEIHERHOF_LATITUDE = 49.4558
WEIHERHOF_LONGITUDE = 10.9274
API_KEY = '40f2897e616f6fa1d84d524176fac1ca'

WIND_CLOSE_LIMIT = 15.0

def cgi_main(config):
    import cgitb
    cgitb.enable()

    debug("Running as CGI script")

    print("Content-Type: text/html; charset=utf-8")
    print()

    print("<html><head><title>Fenstersteuerung</title>")
    print("<meta name='viewport' content='initial-scale=1.2'>")
    print("</head>")
    print("<body>")

    print('<h1>Dachfenstersteuerung Gutenbergstraße</h1>')
    execute_actions(config)

    print_status(config)
    print_actions(config)
    print_last_events(config)
    print_documentation(config)

def is_manual_open_or_close(config):
    return config['manual_open_close'] == today_day()

def print_status(config):
    print('<h2>Status</h2>')
    print('<ul>')
    if config['is_open'] and is_manual_open_or_close(config):
        print('<li>Das Fenster ist <b>manuell geöffnet.</b></li>')
    elif config['is_open']:
        print('<li>Das Fenster ist <b>automatisch geöffnet.</b></li>')
    elif not config['is_open'] and is_manual_open_or_close(config):
        print('<li>Das Fenster ist <b>manuell geschlossen.</b></li>')
    else:
        print('<li>Das Fenster ist <b>automatisch geschlossen.</b></li>')

    if config['auto_mode']:
        print('<li>Das automatische Öffnen per Zeitsteuerung ist <b>aktiv.</b></li>')
    else:
        print('<li>Das automatische Öffnen per Zeitsteuerung ist <b>nicht aktiv.</b></li>')
    print('</ul>')

def print_actions(config):
    print('<h2>Aktionen</h2>')
    print('<table border="0"><tr><td>')
    print(' <form method="post" name="open"><input type="hidden" name="action" value="open" /> '
          '   <input type="submit" value="Fenster öffnen" /> </form>')
    print('</td><td>')
    print(' <form method="post" name="close"><input type="hidden" name="action" value="close" /> '
          '   <input type="submit" value="Fenster schließen" /> </form>')
    print('</td></tr></tr><td colspan="2">')
    if config['auto_mode']:
        print(' <form method="post" name="set_auto_mode">'
              '  <input type="hidden" name="set_auto_mode" value="0" /> '
              '   <input type="submit" value="Zeitsteuerung deaktivieren" /> </form>')
    else:
        print(' <form method="post" name="set_auto_mode">'
              '  <input type="hidden" name="set_auto_mode" value="1" /> '
              '   <input type="submit" value="Zeitsteuerung aktivieren" /> </form>')
    print('</td></tr></table>')

def print_last_events(config):
    last_events = get_last_events()
    if len(last_events.strip()) == 0:
        return
    print("<h2>Letzte Ereignisse</h2>")
    print("<pre>")
    print(get_last_events())
    print("</pre></body></html>")

def print_documentation(config):
    print("<h2>Infos zur Steuerung</h2>")
    print("""
<h3>Erkennung des Zustandes</h3>
<p>Die Steuerung hat keine Rückkopplung zum tatsächlichen Fensterzustand, d. h. wenn das
Fenster mit der Fernbedienung geöffnet oder geschlossen wurde, kann sie dies nicht erkennen
und das Fenster wird ggf. auch nicht geschlossen. Ebenfalls ist die Anzeige des
Zustandes falsch, wenn das Fenster durch den im Fenster eingebauten Regensensor
geschlossen wurde. Dies ist aber insofern unproblematisch als das bei Regen die
Steuerung dann ohnehin zeitverzögert das Fenster schließt.
</p>

<h3>Wetterdaten</h3>
<p>Die Wetterstation auf meinem Balkon liefert ca. alle drei Minuten einen Impuls mit
der aktuellen Temperatur, der aktuellen Luftfeuchte und Windgeschwindigkeit sowie einen
Niederschlagswert (basierend auf einer Wippe, welche bei 0,3 mm Niederschlag einen
Impuls liefert).</p>

<h3>Automatisches Schließen</h3>
<p>Unabhängig ob das Fenster manuell über das Webinterface oder durch die Zeitsteuerung
geöffnet wurde, wird das Fenster bei folgenden Bedingungen sofort geschlossen:</p>
<ul>
  <li>Die Windgeschwindigkeit ist größer als 15 km/h</li>
  <li>Es wurde Niederschlag festgestellt.</li>
</ul>

<h3>Zeitsteuerung</h3>

<p>Zunächst gibt es ein Zeitintervall, wann das Fenster laut Steuerung geöffnet sein soll.
Dieses Zeitintervall unterscheidet sich zwischen Winter- und Sommerbetrieb.
</p>

<ul>
<li>Die Steuerung meldet <i>Winterbetrieb</i>, wenn die vorhergesagte aktuelle Temperatur
für den heutigen Tag größer als 20 °C ist. Als Vorhersagequelle wird
<a href="{WEIHERHOF_URL}">Forecast.io</a> verwendet.<br>

Im Winterbetrieb wird das Fenster eine Stunde nach Sonnenaufgang und eine Stunde vor
Sonnenuntergang geöffnet und ansonsten geschlossen.
</li>
<li>Die Steuerung meldet <i>Sommerbetrieb</i>, wenn die maximale Temperatur für den heutigen
Tag größer als 20 °C sein wird.<br>

Im Sommerbetrieb ist das Fenster zwischen Sonnenaufgang und Sonnenuntergang den ganzen
Tag offen.
</li>
</ul>

<p>Folgende Bedingungen verhindern das automatische Öffnen (anders als die oben unter
<i>Automatisches Schließen</i> aufgelisteten Bedingungen führen sie nicht zum Schließen
bei manuell geöffnetem Fenster):</p>

<ul>
  <li>Die Windgeschwindigkeit in der letzten Stunde war bei einer Messung größer als 15 km/h.</li>
  <li>Es hat in der letzten Stunde geregnet.</li>
  <li>Die Temperatur ist unter 0,0 °C. Dies soll Schäden durch Anfrieren verhindern.</li>
</ul>


    """.format(WEIHERHOF_URL=WEIHERHOF_URL))

def execute_actions(config):
    form = cgi.FieldStorage()

    if 'action' in form:
        if form['action'].value == 'open':
            now = datetime.datetime.now()
            if now.hour <= 6 or now.hour >= 19:
                print('<hr><b>FEHLER:</b> Das manuelle Öffnen zwischen 19 und 6 Uhr ist aus Sicherheitsgründen '
                        'nicht möglich!<hr>')
            else:
                open_window(config, 'Manuelle Aktion', True)
        elif form['action'].value == 'close':
            close_window(config, 'Manuelle Aktion', True)
    if 'set_auto_mode' in form:
        config['auto_mode'] = bool(int(form['set_auto_mode'].value))

def log_action(string):
    debug('ACTION: ' + string)
    global EVENT_LOG
    f = open(EVENT_LOG, 'a')
    timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    f.write(timestamp + " " + string + "\n")
    f.close()

def get_last_events():
    global EVENT_LOG
    if not os.path.exists(EVENT_LOG):
        return ''

    f = open(EVENT_LOG, 'r')
    lines = f.readlines()
    f.close()
    n = min(len(lines), 10)
    return ''.join(reversed(lines[-n:]))

def window_command(command_letter):
    global PORT
    global DRY_RUN
    if DRY_RUN:
        return
    import serial
    ser = serial.Serial(PORT, 9600)
    ser.write((command_letter + "\r\n").encode())
    ser.close()

def today_day(zero=False):
    """ If zero is true, returns 0. Otherwise the function returns the day.
        This function is to record manual open/close actions.
    """
    if zero:
        return 0
    else:
        tmstruct = time.localtime()
        return tmstruct.tm_mday

def today_wday_en():
    """ Returns the English name of the current weekday """
    names = [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' ]
    tmstruct = time.localtime()
    return names[tmstruct.tm_wday]

def fetch_max_temp_today(lat, log):
    try:
        URL_TEMPLATE = 'https://api.forecast.io/forecast/{api_key}/{lat},{log},{time}?units=si'

        tmstruct = time.localtime(time.time()+(1*24*3600))
        timestring = time.strftime("%Y-%m-%dT%H:%M:%S%z", tmstruct)
        url = URL_TEMPLATE.format(lat=lat, log=log, time=timestring, api_key=API_KEY)
        context = ssl._create_unverified_context()
        json_data = urlrequest.urlopen(url, context=context).read()
        json_decoded = json.loads(json_data.decode('utf-8'))
        max_temp = json_decoded['daily']['data'][0]['temperatureMax']
        debug('max temp for {0} is {1}'.format(timestring, max_temp))
        return max_temp
    except Exception as e:
        debug('Error when fetching weather: %s' % (e))
        pass

    # that's the error case :-)
    return -273

def open_window(config, reason, force=False):
    if config['is_open'] and not force:
        return
    try:
        window_command('u')
        log_action("Fenster wurde geöffnet, Grund: %s" % (reason))
        config['is_open'] = True
        config['manual_open_close'] = today_day(not force)
    except OSError as e:
        log_action("Fehler beim Öffnen: %s" % (str(e)))

def close_window(config, reason, force=False):
    if not config['is_open'] and not force:
        return
    try:
        window_command('d')
        log_action("Fenster wurde geschlossen, Grund: %s" % (reason))
        config['is_open'] = False
        config['manual_open_close'] = today_day(not force)
    except OSError as e:
        log_action("Fehler beim Öffnen: %s" % (str(e)))

def DEG2RAD(x):
    """ Converts degrees to radiant """
    return x * math.pi / 180.0

def getTimeZoneHours():
    return -(time.altzone if time.daylight else time.timezone)/(60*60)

def calculate_sunrise_sunset(latitude, longitude):
    """ Calculates sunrise and sunset time for a given location at with latitude/longitude.
    Returns a tuple of two tm_struct object. The first is the sunrise, the second the
    sunrise. Both are localtime.  """

    return (calculate_sunrise_sunset_internal(time.time(), latitude, longitude, False),
            calculate_sunrise_sunset_internal(time.time(), latitude, longitude, True))

def calculate_sunrise_sunset_internal(date, latitude, longitude, sunset, iteration=0):

    height = -0.84 # stands for sunset/sunrise, nautical twilight would be 12.0

    # double x       => Jahreszeit
    # double phi     => Winkel der Erde auf der Bahn zur Sonne
    # double delta   => Deklination
    # double psi     => Rektaszension
    # double tau     => Sundenwinekl der Sonne fuer die Hohe
    # double u       => Zeit seit Mitternacht, der zu dieser Stundenwinkel erreicht wird

    TU = (365.2422 * 86400)
    EPSILON = 0.0167
    GAMMA = DEG2RAD(103)
    ALPHA = DEG2RAD(23.44)
    T0 = 962730000

    x = 2 * math.pi / TU * (date - T0);

    # make 0 <= x <= 2*PI
    x = math.fmod(x, 2*math.pi)
    if (x < 0):
        x += 2*math.pi

    phi = GAMMA + x - 2 * EPSILON * math.sin(x)
    delta = math.asin(math.sin(ALPHA) * math.sin(phi))
    psi = math.pi + math.atan2(-math.cos(ALPHA)*math.sin(phi), -math.cos(phi))
    tau = math.acos( (math.sin(DEG2RAD(height)) - math.sin(delta)*math.sin(DEG2RAD(latitude)))
            / (math.cos(delta) * math.cos(DEG2RAD(latitude))))

    if not sunset:
        tau *= -1

    u = math.fmod((24/(2*math.pi) * (tau + psi - GAMMA - x - DEG2RAD(longitude))
        + getTimeZoneHours() + 12), 24)
    if u < 0:
        u = 24 + u

    # konvertieren
    tmstruct = time.localtime(date)
    tmstruct_w = list(tmstruct)
    tmstruct_w[3] = int(u)
    tmstruct_w[4] = int(round((u - int(u)) * 60))
    tmstruct = time.struct_time(tuple(tmstruct_w))

    if iteration < 1:
        iteration += 1
        t = time.mktime(tmstruct)
        return calculate_sunrise_sunset_internal(t , latitude, longitude, sunset, iteration)
    else:
        return tmstruct

def read_configuration():
    """ Reads the configuration. Returns a dictionary
          { 'is_open'           : bool,
            'manual_open_close' : int,
            'auto_mode'         : bool } """
    global CONFIGURATION_FILE
    try:
        with open(CONFIGURATION_FILE, 'r') as fp:
            return json.load(fp)
    except:
        # return some default values
        return { 'is_open'           : False,
                 'manual_open_close' : 0,
                 'auto_mode'         : True }

def write_configuration(config):
    """ Writes the configuration. Expects a dictionary as documented in read_configuration() """
    global CONFIGURATION_FILE
    with open(CONFIGURATION_FILE, 'w') as fp:
        json.dump(config, fp)
        fp.write('\n')

def update_weather(config):
    """Fetches the weather from Yahoo and updates the configuration"""
    temp = fetch_max_temp_today(WEIHERHOF_LATITUDE, WEIHERHOF_LONGITUDE)
    if temp > -273:
        config['max_temp'] = temp
        config['max_temp_day'] = today_day()
    else:
        # delete the keys so that we try it next time
        if 'max_temp' in config:
            config.pop('max_temp')
        if 'max_temp_day' in config:
            config.pop('max_temp_day')
    write_configuration(config)

def in_summer_mode(config):
    """Checks if we are in summer mode"""
    if 'max_temp' not in config or 'max_temp_day' not in config:
        update_weather(config)
    if 'max_temp_day' in config and config['max_temp_day'] != today_day():
        update_weather(config)

    summer = 'max_temp' in config and config['max_temp'] >= 20
    debug('summer=%s' % (summer))
    return summer

def check_auto(config):

    # are we in auto mode?
    if not config['auto_mode']:
        debug('Auto mode is disabled')
        return

    # do some checks if we need to close the window
    if config['is_open']:

        current_wind_speed = float(os.environ['VETERO_CURRENT_WIND'])
        debug('Current wind: %.1f' % (current_wind_speed))
        global WIND_CLOSE_LIMIT
        if current_wind_speed > WIND_CLOSE_LIMIT:
            debug('Wind speed greater than %.1f km/h, closing window' % (WIND_CLOSE_LIMIT))
            close_window(config, 'Windgeschwindigkeit %.1f km/h' % (current_wind_speed))
            return

        current_rain = int(os.environ['VETERO_CURRENT_RAIN'])
        debug('Current rain: %d' % (current_rain))
        if current_rain:
            debug('It\'s raining, close the window')
            close_window(config, 'Niederschlag')
            return

    # now check if we should open the window in auto mode

    sunset_sunrise = calculate_sunrise_sunset(WEIHERHOF_LATITUDE, WEIHERHOF_LONGITUDE)
    sunrise = time.mktime(sunset_sunrise[0])
    sunset = time.mktime(sunset_sunrise[1])

    if 'VELUXCTRL_NOW' in os.environ:
        now = int(os.environ['VELUXCTRL_NOW'])
    else:
        now = time.time()

    open_time = False
    if in_summer_mode(config):
        if now < sunset and now > sunrise:
            open_time = True
    else:
        if now > sunrise and now < (sunrise + 60*60):
            open_time = True
        elif now < sunset and now > (sunset - 60*60):
            open_time = True

    debug('open_time=%s' % (open_time))

    if open_time:
        result_string = run_vetero_query(
                "SELECT MAX(rain), MAX(wind)/100.0 FROM weatherdata "
                "WHERE (julianday('now') - julianday(timestamp)) < 0.015")
        result_array = result_string.split('\t')
        rain_hour = int(result_array[0])
        max_wind_hour = float(result_array[1])
        current_temp = float(os.environ['VETERO_CURRENT_TEMPERATURE'])

        debug('max_wind_hour=%s' % (max_wind_hour))
        debug('rain_hour=%s' % (rain_hour))
        debug('current_temp=%s' % (current_temp))

        if max_wind_hour > WIND_CLOSE_LIMIT or rain_hour > 0 or current_temp < 0:
            debug('Don\'t open because of wind, rain or temperature')
            return
        if not is_manual_open_or_close(config):
            open_window(config, 'Zeitsteuerung')
    else:
        if not is_manual_open_or_close(config):
            close_window(config, 'Zeitsteuerung')


def run_vetero_query(query):
    return subprocess.check_output(['vetero-db', '-m', '-r', query]).decode()


def cli_main(config):
    parser = optparse.OptionParser()
    (options, args) = parser.parse_args()
    if len(args) != 1:
        print("Missing command")
        return

    command = args[0]
    debug("CLI: mode=%s" % (command))
    if command == 'sunset_rise':
        dt = calculate_sunrise_sunset(WEIHERHOF_LATITUDE, WEIHERHOF_LONGITUDE)
        print('%02d:%02d %02d:%02d' % (dt[0].tm_hour, dt[0].tm_min, dt[1].tm_hour, dt[1].tm_min))
    elif command == 'show_config':
        print('Auto mode: %(auto_mode)s' % (config))
        print('Currently open: %(is_open)s' % (config))
        print('Manual open/close day: %(manual_open_close)s' % (config))
    elif command == 'open':
        open_window(config, 'Manuelle Aktion (Kommandozeile)', True)
    elif command == 'close':
        close_window(config, 'Manuelle Aktion (Kommandozeile)', True)
    elif command == 'check_auto':
        os.system('env > /tmp/veluxctrl')
        check_auto(config)
    elif command == 'fetch_temp':
        print('Temperatur: ' + str(fetch_max_temp_today(WEIHERHOF_LATITUDE, WEIHERHOF_LONGITUDE)))
    else:
        if command != 'help':
            print('Invalid command.')
            print('')
        print('Valid commands are: ')
        print('   sunset_rise    - shows sunset and sunrise times')
        print('   show_config    - shows the current configuration')
        print('   check_auto     - triggers the automatic checks (run from veterod)')
        print('   fetch_temp     - Fetches the maximum temperature for tomorrow')
        print('   open           - open the window')
        print('   close          - close the window')

logfile = None
def debug(string):
    global DEBUG_LOG
    global logfile
    global WRITE_DEBUG_LOG

    timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    sys.stderr.write(timestamp + " " + string + "\n")
    sys.stderr.flush()

    if not WRITE_DEBUG_LOG:
        return

    if not logfile:
        logfile = open(DEBUG_LOG, 'a')
    logfile.write(timestamp + " " + string + "\n")
    logfile.flush()

def main():
    debug("-------------------- Startup ------------------------")
    config = read_configuration()

    if 'SERVER_NAME' in os.environ:
        cgi_main(config)
    else:
        cli_main(config)

    write_configuration(config)

if __name__ == '__main__':
    main()
