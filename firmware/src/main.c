/******************************************************************************
 * Software UART example for MSP430.
 *
 * Stefan Wendler
 * sw@kaltpost.de
 * http://gpio.kaltpost.de
 *
 * Echos back each character received enclosed in square brackets "[" and "]".
 * Use /dev/ttyACM0 at 9600 Bauds (and 8,N,1).
 ******************************************************************************/

#include <msp430.h>
#include <stdint.h>

#include "uart.h"

#define UP                  BIT6
#define DOWN                BIT0
#define STOP                BIT5
#define SIGNAL_DIR          P1DIR
#define SIGNAL_OUT          P1OUT

void pin_init(void)
{
    SIGNAL_DIR |= STOP + UP + DOWN;
    SIGNAL_OUT &= ~(STOP + UP + DOWN); // off by default
}

void delay(void)
{
	volatile int i;
	for(i = 0; i < 32000; i++);
}

void press_key(uint8_t val)
{
    SIGNAL_OUT |= val;
    delay();
    SIGNAL_OUT &= ~val;
}

int main(void)
{
     WDTCTL = WDTPW + WDTHOLD; 	// Stop WDT

     BCSCTL1 = CALBC1_1MHZ; 		// Set range
     DCOCTL = CALDCO_1MHZ; 		// SMCLK = DCO = 1MHz

     uart_init();
     pin_init();

     __enable_interrupt();

     uart_puts("\n\r***************\n\r");
     uart_puts("VELUX CONTROL\n\r");
     uart_puts("***************\n\r\n\r");

     uint8_t c;

     for (;;) {
          if (!uart_getc(&c))
              continue;

          switch (c) {
          case 'u':
              uart_puts("OK, pressing UP\r\n");
              press_key(UP);
              uart_puts("Finished\r\n");
              break;

          case 'd':
              uart_puts("OK, pressing DOWN\r\n");
              press_key(DOWN);
              uart_puts("Finished\r\n");
              break;

          case 's':
              uart_puts("OK, pressing STOP\r\n");
              press_key(STOP);
              uart_puts("Finished\r\n");
              break;

          default:
              uart_puts("Unknown command received\r\n");
              break;
          }
     }
}

